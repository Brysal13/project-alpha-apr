from django.contrib import admin
from .models import Task


# Register your models here.
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    )
    list_filter = ("is_completed",)
    search_fields = ("name", "project__name", "assignee__username")


admin.site.register(Task, TaskAdmin)
