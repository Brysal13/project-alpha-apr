from django.urls import path
from . import views
from .views import show_my_tasks

app_name = "tasks"

urlpatterns = [
    path("create/", views.create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
